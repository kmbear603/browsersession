﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace BrowserSession
{
    public abstract class BrowserSession : IDisposable
    {
        private IWebDriver _webDriver = null;

        public BrowserSession()
        {
        }

        public Task StartAsync(StartOption option)
        {
            return Task.Run(() =>
            {
                _webDriver = GetWebDriver(option);
            });
        }

        public void Stop()
        {
            ExecuteJavascriptAsync("return window.stop");
        }

        protected abstract IWebDriver GetWebDriver(StartOption option);

        protected static string[] CssExtensions
        {
            get
            {
                return new string[]
                {
                    "css"
                };
            }
        }

        protected static string[] ImageExtensions
        {
            get
            {
                return new string[]
                {
                    "bmp", "gif", "jpg", "jpeg", "png", "tif", "tiff", "svg"
                };
            }
        }

        protected static string[] FontExtensions
        {
            get
            {
                return new string[]
                {
                    "ttf"
                };
            }
        }

        public Task TerminateAsync()
        {
            return Task.Run(() =>
            {
                if (_webDriver != null)
                {
                    _webDriver.Quit();
                    _webDriver.Dispose();
                    _webDriver = null;
                }
            });
        }

        public async void Dispose()
        {
            await TerminateAsync();
        }

        public string Url
        {
            get { return _webDriver.Url; }
        }

        public string Title
        {
            get { return _webDriver.Title; }
        }

        public Task ExecuteJavascriptAsync(string script)
        {
            return ExecuteJavascriptAsync<string>(script);
        }

        public Task<T> ExecuteJavascriptAsync<T>(string script)
        {
            return Task.Run(() =>
            {
                return (T)((IJavaScriptExecutor)_webDriver).ExecuteScript(script);
            });
        }

        public async Task<string> CreateNewTabAsync()
        {
            await ExecuteJavascriptAsync("window.open('about:blank', '_blank');");
            return _webDriver.WindowHandles[_webDriver.WindowHandles.Count - 1];
        }

        public void Maximize()
        {
            _webDriver.Manage().Window.Maximize();
        }

        public void Minimize()
        {
            _webDriver.Manage().Window.Minimize();
        }

        public void FullScreen()
        {
            _webDriver.Manage().Window.FullScreen();
        }

        public System.Drawing.Point WindowPosition
        {
            get { return _webDriver.Manage().Window.Position; }
        }

        public void SetWindowPosition(System.Drawing.Point position)
        {
            _webDriver.Manage().Window.Position = position;
        }

        public System.Drawing.Size WindowSize
        {
            get { return _webDriver.Manage().Window.Size; }
        }

        public void SetWindowSize(System.Drawing.Size size)
        {
            _webDriver.Manage().Window.Size = size;
        }

        public void GoTo(string url)
        {
            _webDriver.Navigate().GoToUrl(url);
        }

        public void Forward()
        {
            _webDriver.Navigate().Forward();
        }

        public void Backward()
        {
            _webDriver.Navigate().Back();
        }

        public void Refresh()
        {
            _webDriver.Navigate().Refresh();
        }

        public void CloseTab(string tab_handle)
        {
            SwitchToTab(tab_handle);
            _webDriver.Close();
        }

        public void SwitchToTab(string tab_handle)
        {
            _webDriver.SwitchTo().Window(tab_handle);
        }

        public string CurrentTabHandle
        {
            get { return _webDriver.CurrentWindowHandle; }
        }

        public string[] AllTabHandles
        {
            get { return _webDriver.WindowHandles.ToArray(); }
        }

        public int TabCount
        {
            get { return _webDriver.WindowHandles.Count; }
        }

        public bool IsCompletedLoading
        {
            get
            {
                return ExecuteJavascriptAsync<string>("return document.readyState").Result == "complete";
            }
        }

        public void SaveScreenshot(string save_path)
        {
            save_path = (!save_path.ToLower().EndsWith(".bmp") ? save_path + ".bmp" : save_path);
            ((ITakesScreenshot)_webDriver).GetScreenshot().SaveAsFile(save_path, ScreenshotImageFormat.Bmp);
        }

        public string PageSource
        {
            get { return _webDriver.PageSource; }
        }

        public WebElement.Frame DefaultFrame
        {
            get { return new WebElement.Frame(_webDriver, null, _webDriver.FindElement(By.XPath("/*"))); }
        }

        public async Task<bool> WaitUntilCompletedLoading(TimeSpan timeout)
        {
            DateTime start = DateTime.Now;
            while (start + timeout > DateTime.Now && !IsCompletedLoading)
                await Task.Delay(100);
#if kill
            if (this is PhantomJSBrowserSession)
            {
                // ugly code for phantomjs to find dynamically loaded content
                System.Drawing.Size original_size = WindowSize;
                SetWindowSize(new System.Drawing.Size(original_size.Width - 1, original_size.Height - 1));
                SetWindowSize(original_size);
            }
#endif // kill
            return IsCompletedLoading;
        }
    }
}
