﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Newtonsoft.Json;

namespace BrowserSession
{
    public class ChromeBrowserSession : BrowserSession
    {
        protected override IWebDriver GetWebDriver(StartOption option)
        {
            var opt = new ChromeOptions();
            if (option.Headless)
                opt.AddArgument("--headless");

            if (!option.LoadImage || !option.LoadCss || !option.LoadFont || option.WildcardOfUrlToBlock != null)
                opt.AddExtension("HTTPRequestBlocker-1.0.2.crx");

            if (option.DefaultHeaders != null)
                opt.AddExtension("ModHeader-2.1.2.crx");

            var driver = new ChromeDriver(opt);

            if (!option.LoadImage || !option.LoadCss || !option.LoadFont || option.WildcardOfUrlToBlock != null)
            {
                // hack into the HTTP Request Blocker extension setting page
                driver.Navigate().GoToUrl("chrome-extension://eckpjmeijpoipmldfbckahppeonkoeko/popup.html");

                List<string> urls = new List<string>();

                Func<string[], string[]> touch_ext = exts =>
                {
                    return (from ext in exts select "\"*://*/*." + ext + "\"").ToArray();
                };

                if (!option.LoadImage)
                    urls.AddRange(touch_ext(ImageExtensions));

                if (!option.LoadCss)
                    urls.AddRange(touch_ext(CssExtensions));

                if (!option.LoadFont)
                    urls.AddRange(touch_ext(FontExtensions));

                if (option.WildcardOfUrlToBlock != null)
                    urls.AddRange((from wc in option.WildcardOfUrlToBlock select "\"" + wc + "\"").ToArray());

                string js = "var backgroundPage = chrome.extension.getBackgroundPage();";
                js += "backgroundPage.save([" + string.Join(",", urls.ToArray()) + "], ()=>{});";
                driver.ExecuteScript(js);

                driver.Navigate().Refresh();
            }

            if (option.DefaultHeaders != null)
            {
                // hack
                // dont navigate to the popup.html
                // because popup.html is an angular webapp which sets localStorage as the input on screen when page unload
                // Here we directly change localStorage without changing anything on screen
                // Therefore if we do our hack on popup.html, our localStorage settings will be reset when page unload

                driver.Navigate().GoToUrl("chrome-extension://idgpnmonknjnojddfkpgkljpfnnfcklj/icon.png");

                string profiles_str;
                {
                    var sb = new StringBuilder();

                    using (var sw = new StringWriter(sb))
                    {
                        using (var jw = new JsonTextWriter(sw))
                        {
                            jw.WriteStartArray();
                            jw.WriteStartObject();

                            jw.WritePropertyName("title");
                            jw.WriteValue("Profile 1");

                            jw.WritePropertyName("hideComment");
                            jw.WriteValue(true);

                            jw.WritePropertyName("headers");
                            jw.WriteStartArray();
                            foreach (var name in option.DefaultHeaders.AllKeys)
                            {
                                jw.WriteStartObject();

                                jw.WritePropertyName("enabled");
                                jw.WriteValue(true);

                                jw.WritePropertyName("name");
                                jw.WriteValue(name);

                                jw.WritePropertyName("value");
                                jw.WriteValue(option.DefaultHeaders[name]);

                                jw.WritePropertyName("comment");
                                jw.WriteValue("");

                                jw.WriteEndObject();
                            }
                            jw.WriteEndArray();

                            jw.WritePropertyName("respHeaders");
                            jw.WriteStartArray();
                            jw.WriteEndArray();

                            jw.WritePropertyName("filters");
                            jw.WriteStartArray();
                            jw.WriteEndArray();

                            jw.WritePropertyName("appendMode");
                            jw.WriteValue("");

                            jw.WriteEndObject();
                            jw.WriteEndArray();
                        }
                    }

                    profiles_str = sb.ToString();
                }

                driver.ExecuteScript("window.localStorage.setItem(\"profiles\", \"" + profiles_str.Replace("\"", "\\\"") + "\");");
                driver.ExecuteScript("window.localStorage.setItem(\"selectedProfile\", 0);");
                driver.ExecuteScript("window.localStorage.setItem(\"selectedTab\", 0);");
            }

            return driver;
        }
    }
}
