﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserSession
{
    class ContextSwitcher : IDisposable
    {
        WebElement.Frame _frame;

        public ContextSwitcher(WebElement.Frame frame)
        {
            _frame = frame;
            frame.SwitchContext();
        }

        public void Dispose()
        {
            _frame.RestoreContext();
        }
    }
}
