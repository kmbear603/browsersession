﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace BrowserSession
{
    public class FirefoxBrowserSession : BrowserSession
    {
        protected override IWebDriver GetWebDriver(StartOption option)
        {
            if (!option.LoadImage || !option.LoadCss || !option.LoadFont || option.WildcardOfUrlToBlock != null || option.DefaultHeaders != null)
                throw new NotImplementedException();

            return new FirefoxDriver();
        }
    }
}
