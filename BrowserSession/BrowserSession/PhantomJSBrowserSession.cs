﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using System.Text.RegularExpressions;

namespace BrowserSession
{
    public class PhantomJSBrowserSession : BrowserSession
    {
        protected override IWebDriver GetWebDriver(StartOption option)
        {
            PhantomJSOptions opt = new PhantomJSOptions();

            if (!string.IsNullOrEmpty(option.DefaultUserAgent))
                opt.AddAdditionalCapability("phantomjs.page.settings.userAgent", option.DefaultUserAgent);

            if (option.DefaultHeaders != null)
            {
                foreach (var key in option.DefaultHeaders.AllKeys)
                {
                    string val = option.DefaultHeaders[key];
                    opt.AddAdditionalCapability("phantomjs.page.customHeaders." + key, val);
                }
            }

            var driver = new PhantomJSDriver(opt);

            if (!option.LoadCss || !option.LoadImage || !option.LoadFont
                || (option.WildcardOfUrlToBlock != null && option.WildcardOfUrlToBlock.Length > 0))
            {
                List<string> abort_list = new List<string>();

                Func<string[], bool> add_ext = exts =>
                {
                    foreach (string ext in exts)
                        abort_list.Add(@"^.+[.]" + ext + "[?]*.*$");
                    return true;
                };

                if (!option.LoadCss)
                    add_ext(CssExtensions);

                if (!option.LoadImage)
                    add_ext(ImageExtensions);

                if (!option.LoadFont)
                    add_ext(FontExtensions);

                if (option.WildcardOfUrlToBlock != null)
                {
                    foreach (var wc in option.WildcardOfUrlToBlock)
                    {
                        var regex = wc.Replace(".", "[.]").Replace("*", ".*").Replace("?", ".");
                        abort_list.Add(regex);
                    }
                }

                string js =
                    "this.onResourceRequested = function(request, net){" +
                    "   const abort_arr = [" + string.Join(",", Array.ConvertAll(abort_list.ToArray(), s => { return "'" + s.ToLower() + "'"; })) + "];" +
                    "   var need_abort = function(url){" +
                    "       for (var i = 0; i < abort_arr.length; i++){" +
                    "           var regex = new RegExp(abort_arr[i]);" +
                    "           if (regex.test(url.toLowerCase()))" +
                    "               return true;" +
                    "       };" +
                    "       return false;" +
                    "   };" +
                    "   if (need_abort(request.url)){" +
                    "       console.log('aborted ' + request.url);" +
                    "       net.abort();" +
                    "   }" +
                    "};";

                driver.ExecutePhantomJS(js);
            }

            driver.ExecutePhantomJS(
                "page.onConsoleMessage = function(msg){" +
                "   console.log(msg);" +
                "};");

            driver.ExecutePhantomJS(
                "page.onError = function(msg, trace){" +
                "   var msgStack = ['ERROR: ' + msg];" +
                "   if (trace && trace.length){" +
                "       msgStack.push('TRACE:');" +
                "       trace.forEach(function(t){" +
                "           msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function \"' + t.function + '\")' : ''));" +
                "       });" +
                "   }" +
                "   console.error(msgStack.join('\n'));" +
                "};");

            return driver;
        }
    }
}
