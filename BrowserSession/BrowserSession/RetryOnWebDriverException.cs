﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace BrowserSession
{
    class RetryOnWebDriverException
    {
        public static void Invoke(Action action)
        {
            int max_retry = 5;
            int sleep = 50;
            WebDriverException ret_ex = null;

            for (int retry = 1; retry <= max_retry; retry++)
            {
                try
                {
                    try
                    {
                        action.Invoke();
                        return;
                    }
                    catch (InvalidOperationException ex)
                    {
                        throw new WebDriverException("InvalidOperationException", ex);
                    }
                }
                catch (WebDriverException ex)
                {
                    ret_ex = ex;
                    if (retry == max_retry)
                        break;
                    System.Threading.Thread.Sleep(sleep * retry);
                }
            }

            throw ret_ex;
        }
    }
}
