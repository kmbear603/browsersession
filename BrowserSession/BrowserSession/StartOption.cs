﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserSession
{
    public class StartOption
    {
        public string DefaultUserAgent = null;
        public System.Collections.Specialized.NameValueCollection DefaultHeaders = null;
        public bool LoadCss = true;
        public bool LoadImage = true;
        public bool LoadFont = true;
        public string[] WildcardOfUrlToBlock = null;
        public bool Headless = false;

        public StartOption Clone()
        {
            StartOption opt = new StartOption();

            opt.DefaultUserAgent = this.DefaultUserAgent;

            if (this.DefaultHeaders != null)
            {
                opt.DefaultHeaders = new System.Collections.Specialized.NameValueCollection();
                foreach (var key in this.DefaultHeaders.AllKeys)
                    opt.DefaultHeaders.Add(key, this.DefaultHeaders[key]);
            }
            else
                opt.DefaultHeaders = null;

            opt.LoadCss = this.LoadCss;

            opt.LoadImage = this.LoadImage;

            opt.LoadFont = this.LoadFont;

            if (this.WildcardOfUrlToBlock != null)
                opt.WildcardOfUrlToBlock = (from wc in this.WildcardOfUrlToBlock select wc).ToArray();
            else
                opt.WildcardOfUrlToBlock = null;

            opt.Headless = this.Headless;

            return opt;
        }
    }
}
