﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace BrowserSession.WebElement
{
    public class Anchor : WebElement
    {
        protected override bool IsMyElement(IWebElement e)
        {
            return string.Compare(e.TagName, "a", true) == 0;
        }

        public string Href
        {
            get
            {
                return GetAttribute("href");
            }
        }
    }
}
