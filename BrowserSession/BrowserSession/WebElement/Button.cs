﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserSession.WebElement
{
    public class Button : Input
    {
        protected override bool IsMyElement(OpenQA.Selenium.IWebElement e)
        {
            if (string.Compare(e.TagName, "button", true) == 0)
                return true;

            string type = e.GetAttribute("type");
            return (string.Compare(e.TagName, "input", true) == 0
                    && Array.Exists(new string[] { "button", "submit", "reset" }, t =>
                    {
                        return string.Compare(type, t, true) == 0;
                    }));
        }
    }
}
