﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace BrowserSession.WebElement
{
    public class CheckBox : Input
    {
        protected override bool IsMyElement(IWebElement e)
        {
            return string.Compare(e.TagName, "input", true) == 0
                    && e.GetAttribute("type") != null
                    && string.Compare(e.GetAttribute("type"), "checkbox", true) == 0;
        }

        public bool IsChecked
        {
            get
            {
                bool ret = false;
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                        ret = element.Selected;
                });
                return ret;
            }
        }

        public void SetChecked(bool check)
        {
            if (!IsChecked)
            {
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                        element.Click();
                });
            }
        }
    }
}
