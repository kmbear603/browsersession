﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace BrowserSession.WebElement
{
    public class Form : WebElement
    {
        protected override bool IsMyElement(IWebElement e)
        {
            return string.Compare(e.TagName, "form", true) == 0;
        }

        public Task SubmitAsync()
        {
            return Task.Run(() =>
            {
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                        element.Submit();
                });
            });
        }
    }
}
