﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace BrowserSession.WebElement
{
    public class Frame : WebElement
    {
        IWebDriver _webDriver;

        public Frame(IWebDriver driver, Frame container_frame, IWebElement element)
        {
            _webDriver = driver;
            base.Init(container_frame, element);
        }

        internal IWebDriver WebDriver
        {
            get { return _webDriver; }
        }

        protected override bool IsMyElement(IWebElement e)
        {
            return string.Compare(e.TagName, "iframe", true) == 0 || string.Compare(e.TagName, "html", true) == 0;
        }

        public void SwitchContext()
        {
            if (containerFrame == null)
                _webDriver.SwitchTo().DefaultContent();
            else
            {
                containerFrame.SwitchContext();
                _webDriver.SwitchTo().Frame(element);
            }
        }

        public void RestoreContext()
        {
            if (containerFrame == null)
            {
                _webDriver.SwitchTo().DefaultContent();
            }
            else
            {
                containerFrame.SwitchContext();
                containerFrame.RestoreContext();
            }
        }

        public override IReadOnlyList<T> FindElementByClassName<T>(string class_name)
        {
            using (new ContextSwitcher(this))
            {
                return _webDriver.FindElements(By.ClassName(class_name)).Select(e =>
                {
                    try
                    {
                        return new T().Init(this, e) as T;
                    }
                    catch (TypeMismatchException)
                    {
                        return null;
                    }
                })
                .Where(we => { return we != null; })
                .ToList();
            }
        }

        public override T FindElementById<T>(string id)
        {
            using (new ContextSwitcher(this))
            {
                try
                {
                    return new T().Init(this, _webDriver.FindElement(By.Id(id))) as T;
                }
                catch (NoSuchElementException)
                {
                    return null;
                }
            }
        }

        public override IReadOnlyList<T> FindElementByTagName<T>(string tag_name)
        {
            using (new ContextSwitcher(this))
            {
                return _webDriver.FindElements(By.TagName(tag_name)).Select(e =>
                {
                    try
                    {
                        return new T().Init(this, e) as T;
                    }
                    catch (TypeMismatchException)
                    {
                        return null;
                    }
                })
                .Where(we => { return we != null; })
                .ToList();
            }
        }

        public override T FindElementByXPath<T>(string xpath)
        {
            using (new ContextSwitcher(this))
            {
                try
                {
                    return new T().Init(this, _webDriver.FindElement(By.XPath(xpath))) as T;
                }
                catch (NoSuchElementException)
                {
                    return null;
                }
            }
        }

        public override IReadOnlyList<T> FindElementByName<T>(string name)
        {
            using (new ContextSwitcher(this))
            {
                return _webDriver.FindElements(By.Name(name)).Select(e =>
                {
                    try
                    {
                        return new T().Init(this, e) as T;
                    }
                    catch (TypeMismatchException)
                    {
                        return null;
                    }
                })
                .Where(we => { return we != null; })
                .ToList();
            }
        }

        public Frame GetFrame(string id)
        {
            using (new ContextSwitcher(this))
            {
                try
                {
                    return new Frame(_webDriver, this, _webDriver.FindElement(By.Id(id)));
                }
                catch (NoSuchElementException)
                {
                    return null;
                }
            }
        }
    }
}
