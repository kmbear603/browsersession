﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace BrowserSession.WebElement
{
    public class Image : WebElement
    {
        protected override bool IsMyElement(IWebElement e)
        {
            return string.Compare(e.TagName, "img", true) == 0;
        }

        public string Src
        {
            get { return GetAttribute("src"); }
        }
    }
}
