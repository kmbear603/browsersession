﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace BrowserSession.WebElement
{
    public class Input : WebElement
    {
        protected override bool IsMyElement(IWebElement e)
        {
            return string.Compare(e.TagName, "input", true) == 0;
        }

        public string Value
        {
            get
            {
                return GetAttribute("value");
            }
        }
    }
}
