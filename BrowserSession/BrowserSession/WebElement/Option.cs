﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace BrowserSession.WebElement
{
    public class Option : WebElement
    {
        protected override bool IsMyElement(IWebElement e)
        {
            return string.Compare(e.TagName, "option", true) == 0;
        }

        public string Value
        {
            get { return GetAttribute("value"); }
        }
    }
}
