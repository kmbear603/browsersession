﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace BrowserSession.WebElement
{
    public class Radio : Input
    {
        protected override bool IsMyElement(IWebElement e)
        {
            return string.Compare(e.TagName, "input", true) == 0
                    && e.GetAttribute("type") != null
                    && string.Compare(e.GetAttribute("type"), "radio", true) == 0;
        }

        public bool IsSelected
        {
            get
            {
                bool ret = false;
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                        ret = element.Selected;
                });
                return ret;
            }
        }

        public void SetSelected(bool selected)
        {
            if (!IsSelected)
            {
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                        element.Click();
                });
            }
        }
    }
}
