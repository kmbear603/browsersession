﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace BrowserSession.WebElement
{
    public class Select : WebElement
    {
        private SelectElement _selectElement;

        protected override bool IsMyElement(IWebElement e)
        {
            return string.Compare(e.TagName, "select", true) == 0;
        }

        private SelectElement SeleniumSelectElement
        {
            get
            {
                if (_selectElement == null)
                    _selectElement = new SelectElement(element);
                return _selectElement;
            }
        }

        public IReadOnlyList<Option> Options
        {
            get
            {
                IReadOnlyList<Option> ret = null;
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                    {
                        List<Option> list = new List<Option>();
                        foreach (var opt in SeleniumSelectElement.Options)
                            list.Add(new Option().Init(containerFrame, opt) as Option);
                        ret = list;
                    }
                });
                return ret;
            }
        }

        public Option FirstSelectedOption
        {
            get
            {
                Option ret = null;
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                    {
                        IWebElement opt = SeleniumSelectElement.SelectedOption;
                        if (opt == null)
                            ret = null;
                        else
                            ret = new Option().Init(containerFrame, opt) as Option;
                    }
                });
                return ret;
            }
        }

        public IReadOnlyList<Option> SelectedOptions
        {
            get
            {
                IReadOnlyList<Option> ret = null;
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                    {
                        List<Option> list = new List<Option>();
                        foreach (var opt in SeleniumSelectElement.AllSelectedOptions)
                            list.Add(new Option().Init(containerFrame, opt) as Option);
                        ret = list;
                    }
                });
                return ret;
            }
        }

        public bool IsMultiSelect
        {
            get
            {
                bool ret = false;
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                        ret = SeleniumSelectElement.IsMultiple;
                });
                return ret;
            }
        }

        public void DeselectAll()
        {
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                    SeleniumSelectElement.DeselectAll();
            });
        }

        public void DeselectByIndex(int index)
        {
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                    SeleniumSelectElement.DeselectByIndex(index);
            });
        }

        public void DeselectByText(string text)
        {
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                    SeleniumSelectElement.DeselectByText(text);
            });
        }

        public void DeselectByValue(string val)
        {
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                    SeleniumSelectElement.DeselectByValue(val);
            });
        }

        public void SelectByIndex(int index)
        {
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                    SeleniumSelectElement.SelectByIndex(index);
            });
        }

        public void SelectByText(string text)
        {
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                    SeleniumSelectElement.SelectByText(text);
            });
        }

        public void SelectByValue(string val)
        {
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                    SeleniumSelectElement.SelectByValue(val);
            });
        }
    }
}
