﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace BrowserSession.WebElement
{
    public class TextBox : Input
    {
        protected override bool IsMyElement(IWebElement e)
        {
            string type = e.GetAttribute("type");
            return (string.Compare(e.TagName, "input", true) == 0
                    && (type == null
                        || Array.Exists(new string[] { "text", "password", "tel", "hidden", "email", "number" }, t =>
                            {
                                return string.Compare(type, t, true) == 0;
                            })
                        )
                    );
        }

        public void SetValue(string val)
        {
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                {
                    element.Clear();
                    element.SendKeys(val);
                }
            });
        }

        public enum SpecialKeyEnum
        {
            Enter, Tab, Home, End, Del
        }

        public void SendSpecialKey(SpecialKeyEnum key)
        {
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                {
                    string key_str;
                    switch (key)
                    {
                        case SpecialKeyEnum.Enter: key_str = Keys.Enter; break;
                        case SpecialKeyEnum.Tab: key_str = Keys.Tab; break;
                        case SpecialKeyEnum.Home: key_str = Keys.Home; break;
                        case SpecialKeyEnum.End: key_str = Keys.End; break;
                        default: throw new NotImplementedException(key.ToString());
                    }
                    element.SendKeys(key_str);
                }
            });
        }

        public void Clear()
        {
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                    element.Clear();
            });
        }
    }
}
