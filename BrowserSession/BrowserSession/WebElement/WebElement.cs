﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace BrowserSession.WebElement
{
    public class WebElement
    {
        protected Frame containerFrame;
        protected IWebElement element;
        private bool _inited;

        public WebElement()
        {
            _inited = false;
        }

        protected class TypeMismatchException : Exception
        {
            public TypeMismatchException()
                : base()
            {
            }

            public TypeMismatchException(string message)
                            : base(message)
            {
            }
        }

        internal virtual WebElement Init(Frame container_frame, IWebElement element)
        {
            if (!IsMyElement(element))
                throw new TypeMismatchException();
            containerFrame = container_frame;
            this.element = element;
            _inited = true;
            return this;
        }

        protected virtual bool IsMyElement(IWebElement e)
        {
            return true;
        }

        public bool HasAttribute(string attr_name)
        {
            return GetAttribute(attr_name) != null;
        }

        public string GetAttribute(string attr_name)
        {
            string ret = null;
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                    ret = element.GetAttribute(attr_name);
            });
            return ret;
        }

        public IReadOnlyList<string> ClassNames
        {
            get
            {
                return GetAttribute("class").Split(' ');
            }
        }

        public bool HasClass(string class_name)
        {
            return ClassNames.Contains(class_name);
        }

        public string TagName
        {
            get
            {
                string ret = null;
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                        ret = element.TagName;
                });
                return ret;
            }
        }

        public string Name
        {
            get { return GetAttribute("name"); }
        }

        public string Id
        {
            get
            {
                return GetAttribute("id");
            }
        }

        public string Text
        {
            get
            {
                string ret = null;
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                    {
                        if (element.Displayed)
                            ret = element.Text;
                        else
                            ret = element.GetAttribute("textContent");
                    }
                });
                return ret;
            }
        }

        public bool IsVisibleOnWindow
        {
            get
            {
                bool ret = false;
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                        ret = element.Displayed;
                });
                return ret;
            }
        }

        public bool IsEnabled
        {
            get
            {
                bool ret = false;
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                        ret = element.Enabled;
                });
                return ret;
            }
        }

        public System.Drawing.Point Position
        {
            get
            {
                System.Drawing.Point ret = System.Drawing.Point.Empty;
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                        ret = element.Location;
                });
                return ret;
            }
        }

        public System.Drawing.Size Size
        {
            get
            {
                System.Drawing.Size ret = System.Drawing.Size.Empty;
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                        ret = element.Size;
                });
                return ret;
            }
        }

        public Task ClickAsync()
        {
            return Task.Run(() =>
            {
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                    {
                        ScrollIntoViewAsync().Wait();
                        element.Click();
                    }
                });
            });
        }

        public Task ClickWithoutScrollAsync()
        {
            return Task.Run(() =>
            {
                RetryOnWebDriverException.Invoke(() =>
                {
                    using (new ContextSwitcher(containerFrame))
                    {
                        element.Click();
                    }
                });
            });
        }

        public WebElement FindElementByXPath(string xpath)
        {
            return FindElementByXPath<WebElement>(xpath);
        }

        public virtual T FindElementByXPath<T>(string xpath) where T : WebElement, new()
        {
            T ret = null;
            RetryOnWebDriverException.Invoke(() =>
            {
                try
                {
                    using (new ContextSwitcher(containerFrame))
                        ret = new T().Init(containerFrame, element.FindElement(By.XPath(xpath))) as T;
                }
                catch (NoSuchElementException)
                {
                    ret = null;
                }
            });
            return ret;
        }

        public WebElement FindElementById(string id)
        {
            return FindElementById<WebElement>(id);
        }

        public virtual T FindElementById<T>(string id) where T : WebElement, new()
        {
            T ret = null;
            RetryOnWebDriverException.Invoke(() =>
            {
                try
                {
                    using (new ContextSwitcher(containerFrame))
                        ret = new T().Init(containerFrame, element.FindElement(By.Id(id))) as T;
                }
                catch (NoSuchElementException)
                {
                    ret = null;
                }
            });
            return ret;
        }

        public IReadOnlyList<WebElement> FindElementByClassName(string class_name)
        {
            return FindElementByClassName<WebElement>(class_name);
        }

        public virtual IReadOnlyList<T> FindElementByClassName<T>(string class_name) where T :WebElement,new()
        {
            IReadOnlyList<T> ret = null;
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                {
                    ret = element.FindElements(By.ClassName(class_name)).Select(e =>
                    {
                        try
                        {
                            return new T().Init(containerFrame, e) as T;
                        }
                        catch (TypeMismatchException)
                        {
                            return null;
                        }
                    })
                    .Where(we => { return we != null; })
                    .ToList();
                }
            });
            return ret;
        }

        public virtual IReadOnlyList<WebElement> FindElementByTagName(string tag_name)
        {
            return FindElementByTagName<WebElement>(tag_name);
        }

        public virtual IReadOnlyList<T> FindElementByTagName<T>(string tag_name) where T : WebElement, new()
        {
            IReadOnlyList<T> ret = null;
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                {
                    ret = element.FindElements(By.TagName(tag_name)).Select(e =>
                    {
                        try
                        {
                            return new T().Init(containerFrame, e) as T;
                        }
                        catch (TypeMismatchException)
                        {
                            return null;
                        }
                    })
                    .Where(we => { return we != null; })
                    .ToList();
                }
            });
            return ret;
        }

        public virtual IReadOnlyList<WebElement> FindElementByName(string name)
        {
            return FindElementByName<WebElement>(name);
        }

        public virtual IReadOnlyList<T> FindElementByName<T>(string name) where T : WebElement, new()
        {
            IReadOnlyList<T> ret = null;
            RetryOnWebDriverException.Invoke(() =>
            {
                using (new ContextSwitcher(containerFrame))
                {
                    ret = element.FindElements(By.Name(name)).Select(e =>
                    {
                        try
                        {
                            return new T().Init(containerFrame, e) as T;
                        }
                        catch (TypeMismatchException)
                        {
                            return null;
                        }
                    })
                    .Where(we => { return we != null; })
                    .ToList();
                }
            });
            return ret;
        }

        public string InnerHTML
        {
            get
            {
                return GetAttribute("innerHTML");
            }
        }

        public string OuterHTML
        {
            get
            {
                return GetAttribute("outerHTML");
            }
        }

        public async Task ScrollIntoViewAsync()
        {
            ((IJavaScriptExecutor)this.containerFrame.WebDriver).ExecuteScript("arguments[0].scrollIntoView(true);", this.element);
            await Task.Delay(500);
        }
    }
}
