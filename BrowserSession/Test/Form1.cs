﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrowserSession;

namespace Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            using (var browser = new ChromeBrowserSession())
            {
                var opt = new StartOption()
                {
                    LoadImage = false,
                    LoadCss = false,
                };

                await browser.StartAsync(opt);

                browser.GoTo("https://www.google.com");

                while (!browser.IsCompletedLoading)
                    await Task.Delay(1000);

                var doc = browser.DefaultFrame;
                var body = doc.FindElementByTagName("body")[0];

                var form = body.FindElementById<BrowserSession.WebElement.Form>("tsf");

                var search_field = form.FindElementByXPath<BrowserSession.WebElement.TextBox>("//*[@id=\"lst-ib\"]");
                search_field.SetValue("Hello");

                await form.SubmitAsync();

                browser.SaveScreenshot("test");
                Console.WriteLine(browser.Title);
                Console.WriteLine(browser.Url);
            }
        }
    }
}
