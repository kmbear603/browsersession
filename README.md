# BrowserSession
A desktop C# library designed to manipulate Selenium WebDriver and WebElements in a more OO style.

## Currently supported browsers
* Google Chrome
* PhantomJS

## Projects in the solution
* BrowserSession
* Test - to demonstrate basic feature of BrowserSession

## How to use
```csharp
using (var browser = new ChromeBrowserSession())
{
    var opt = new StartOption()
    {
        LoadImage = true,
        LoadCss = true,
    };

    await browser.StartAsync(opt);

    browser.GoTo("https://www.google.com");

    while (!browser.IsCompletedLoading)
        await Task.Delay(1000);

    var doc = browser.DefaultFrame;
    var body = doc.FindElementByTagName("body")[0];

    var form = body.FindElementById<Form>("tsf");

    var search_field = form.FindElementByXPath<TextBox>("//*[@id=\"lst-ib\"]");
    search_field.SetValue("Hello");

    await form.SubmitAsync();

    browser.SaveScreenshot("test");
    Console.WriteLine(browser.Title);
    Console.WriteLine(browser.Url);
}
```

## Author
kmbear603@gmail.com
